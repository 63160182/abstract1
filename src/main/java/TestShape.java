/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestShape {
    public static void main(String[] args){
        Circle c1=new Circle(1.5);
        Circle c2=new Circle(4.5);
        Circle c3=new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        Rectangle r1=new Rectangle(3,4);
        System.out.println(r1);
        Square s1=new Square(5);
        System.out.println(s1);
        Shape[] shapes ={c1,c2,c3,r1,s1};
        for(int i=0; i<shapes.length;i++){
            System.out.println(shapes[i].getName()+" area :"+shapes[i].calArea());
        }
    }
}
